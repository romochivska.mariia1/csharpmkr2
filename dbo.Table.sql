﻿CREATE TABLE [dbo].[School]
(
    [surname] TEXT NOT NULL, 
    [gender] TEXT NULL, 
    [class] NVARCHAR(50) NULL, 
    [mark1] INT NULL, 
    [mark2] INT NULL, 
    CONSTRAINT [PK_School] PRIMARY KEY ([surname])
)
