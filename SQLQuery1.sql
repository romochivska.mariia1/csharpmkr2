﻿INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Doe', 'Male', '10A', 80, 90);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Smith', 'Female', '11B', 78, 89);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Johnson', 'Male', '9C', 95, 88);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Wilson', 'Female', '9A', 90, 88);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Davis', 'Male', '10B', 85, 90);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Taylor', 'Male', '10A', 82, 96);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Brown', 'Male', '11C', 78, 94);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Williams', 'Female', '12A', 92, 87);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Martinez', 'Female', '12B', 94, 89);
INSERT INTO School (surname, gender, class, mark1, mark2)
VALUES ('Lee', 'Male', '11A', 88, 91);