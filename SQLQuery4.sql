﻿DECLARE @ClassX NVARCHAR(255) = '11B';

SELECT AVG((mark1 + mark2) / 2.0) AS AverageMark
FROM School
WHERE Class = @ClassX;
